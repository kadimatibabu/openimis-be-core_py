# Generated by Django 3.0.14 on 2021-06-01 12:51

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0012_users_officers_admins'),
    ]

    operations = [
        migrations.RenameField(
            model_name='usermutation',
            old_name='user',
            new_name='core_user',
        ),
    ]
